package konfig

import "reflect"

type Field interface {
	GetString(converted bool) string
	SetString(value string, converted bool)
}

func castField(v reflect.Value) (field Field, ok bool) {
	if v.CanInterface() {
		field, ok = v.Interface().(Field)
	}
	if !ok && v.CanAddr() {
		field, ok = v.Addr().Interface().(Field)
	}
	return
}
