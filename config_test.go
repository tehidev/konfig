package konfig_test

import (
	"bytes"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/knopkalab/go/konfig"
)

type String2 string

type TestConfig struct {
	konfig.Value

	String  string
	String2 String2
	Int     int `desc:"this is 2 field"`
	Uint16  uint16
	Struct  struct {
		F1 konfig.Base64
		F2 string `config:"-"`
		F3 string
	}
	StackStruct      TestConfigChild
	lowerStackStruct TestConfigChild
	PtrStruct        *TestConfigChild
	TestConfigChild
	SliceField []int
	HTTPPort   uint16
	FloatVal32 float32
	FloatVal64 float64
	Bool       bool
	Bytes      []byte
	Strings    []string
	Ints       []int64
	Dur        time.Duration
}

type TestConfigChild struct {
	EmbedInt64 int64
	EmbedInt   int
}

func TestConfigs(t *testing.T) {
	conf := &TestConfig{
		String:           "str",
		Uint16:           18,
		SliceField:       []int{1, 2, 3},
		lowerStackStruct: TestConfigChild{},
	}

	conf.String2 = String2("str123")

	conf.EmbedInt = 5
	conf.Dur = time.Millisecond * 1500
	conf.Struct.F1 = "123"

	var buf bytes.Buffer

	assert.NoError(t, konfig.Marshal(conf, &buf))

	assert.Equal(t, configWithDefaults, buf.String())

	conf = &TestConfig{}
	assert.NoError(t, konfig.Unmarshal(conf, &buf))
	assert.NoError(t, konfig.Marshal(conf, &buf))

	assert.Equal(t, configWithoutDefaults, buf.String())

	conf.SetDefaults()
	buf.Reset()
	assert.NoError(t, konfig.Marshal(conf, &buf))
	assert.Equal(t, configDefaults, buf.String())
}

const configWithDefaults = `# default: str
string = str
# default: str123
string_2 = str123
# this is 2 field. Default: 0
int = 0
# default: 18
uint_16 = 18

# default: 123
struct.f_1 = MTIz
struct.f_3 = 

# default: 0
stack_struct.embed_int_64 = 0
# default: 0
stack_struct.embed_int = 0

# default: 0
ptr_struct.embed_int_64 = 0
# default: 0
ptr_struct.embed_int = 0

# default: 0
embed_int_64 = 0
# default: 5
embed_int = 5
# default: [1,2,3]
slice_field = [1,2,3]
# default: 0
http_port = 0
# default: 0
float_val_32 = 0
# default: 0
float_val_64 = 0
# default: false
bool = false
# default: []
bytes = []
# default: []
strings = []
# default: []
ints = []
# default: 1.5s
dur = 1.5s
`

const configWithoutDefaults = `string = str
string_2 = 
# this is 2 field. Default: 0
int = 0
# default: 0
uint_16 = 18

struct.f_1 = MTIz
struct.f_3 = 

# default: 0
stack_struct.embed_int_64 = 0
# default: 0
stack_struct.embed_int = 0

# default: 0
ptr_struct.embed_int_64 = 0
# default: 0
ptr_struct.embed_int = 0

# default: 0
embed_int_64 = 0
# default: 0
embed_int = 5
# default: []
slice_field = [1,2,3]
# default: 0
http_port = 0
# default: 0
float_val_32 = 0
# default: 0
float_val_64 = 0
# default: false
bool = false
# default: []
bytes = []
# default: []
strings = []
# default: []
ints = []
# default: 0s
dur = 1.5s
`

const configDefaults = `string = 
string_2 = 
# this is 2 field. Default: 0
int = 0
# default: 0
uint_16 = 0

struct.f_1 = 
struct.f_3 = 

# default: 0
stack_struct.embed_int_64 = 0
# default: 0
stack_struct.embed_int = 0

# default: 0
ptr_struct.embed_int_64 = 0
# default: 0
ptr_struct.embed_int = 0

# default: 0
embed_int_64 = 0
# default: 0
embed_int = 0
# default: []
slice_field = []
# default: 0
http_port = 0
# default: 0
float_val_32 = 0
# default: 0
float_val_64 = 0
# default: false
bool = false
# default: []
bytes = []
# default: []
strings = []
# default: []
ints = []
# default: 0s
dur = 0s
`
