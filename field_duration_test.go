package konfig_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/knopkalab/go/konfig"
)

func TestDuration(t *testing.T) {
	d := konfig.NewDuration(time.Hour*24*9 + time.Hour*5 + time.Minute*15 + time.Microsecond*5)
	assert.Equal(t, "9d5h15m5us", d.String())

	d = konfig.NewDuration(0)
	assert.Equal(t, "0s", d.String())

	d = konfig.NewDuration(-time.Minute)
	assert.Equal(t, "-1m", d.String())

	d, ok := konfig.ParseDuration("-9d5h 15m5us")
	assert.True(t, ok)
	assert.Equal(t, "-9d5h15m5us", d.String())

	d, ok = konfig.ParseDuration("")
	assert.False(t, ok)
	assert.Equal(t, "0s", d.String())

	d, ok = konfig.ParseDuration("123")
	assert.True(t, ok)
	assert.Equal(t, "2m3s", d.String())

	d, ok = konfig.ParseDuration("2mf443fW")
	assert.False(t, ok)

	d, ok = konfig.ParseDuration("1d1h1m1s1ms1us1ns")
	assert.True(t, ok)
	assert.Equal(t, "1d1h1m1s1ms1us1ns", d.String())
}
