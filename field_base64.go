package konfig

import (
	"encoding/base64"
	"unicode/utf8"
)

var _ Field = (*Base64)(nil)

type Base64 string

func (s Base64) String() string { return string(s) }

func (s Base64) GetString(converted bool) string {
	switch {
	case s == "":
		return ""
	case converted:
		return base64.StdEncoding.EncodeToString([]byte(s))
	default:
		return string(s)
	}
}

func (s *Base64) SetString(value string, converted bool) {
	if converted && value != "" {
		data, err := base64.StdEncoding.DecodeString(value)
		if err == nil && utf8.Valid(data) {
			value = string(data)
		}
	}
	*s = Base64(value)
}
