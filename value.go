package konfig

import (
	"reflect"
)

type Config interface {
	GetValue() *Value
	SetValue(path, value string, converted, allowEmpty bool) (ok bool)
	SetDefaults()
	ScanValues(converted bool, iter valueField) error
}

func Init(structPtr Config) {
	if structPtr == nil {
		panic("config.Init: structPtr is nil")
	}
	val := structPtr.GetValue()
	if val.i != nil {
		return
	}
	val.i = structPtr
	val.defaults = map[string]string{}

	scanStruct(val.i, func(path string, isStruct bool, tag reflect.StructTag, value reflect.Value) error {
		if !isStruct {
			val.defaults[path] = getStringValue(value, false)
		}
		return nil
	})
}

type Value struct {
	i Config

	defaults map[string]string
}

func (val *Value) GetValue() *Value { return val }

func (val *Value) SetValue(path, value string, converted, allowEmpty bool) (ok bool) {
	scanStruct(val.i, func(iPath string, isStruct bool, tag reflect.StructTag, rVal reflect.Value) error {
		if isStruct || path != iPath {
			return nil
		}
		if value != "" || allowEmpty || tag.Get("empty") == "true" {
			setStringValue(rVal, value, converted)
			ok = true
		}
		return errScanStop
	})
	return
}

func (val *Value) SetDefaults() {
	for path, value := range val.defaults {
		val.SetValue(path, value, false, true)
	}
}

type valueField func(path, desc, value, defaultValue string, typ reflect.Type) error

func (val *Value) ScanValues(converted bool, iter valueField) error {
	return scanStruct(val.i, func(path string, isStruct bool, tag reflect.StructTag, value reflect.Value) error {
		if isStruct {
			return nil
		}
		strVal := getStringValue(value, converted)
		return iter(path, tag.Get("desc"), strVal, val.defaults[path], value.Type())
	})
}
