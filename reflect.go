package konfig

import (
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"

	"gitlab.com/knopkalab/go/utils"
)

func getStringValue(val reflect.Value, converted bool) string {
	if field, ok := castField(val); ok {
		return field.GetString(converted)
	}
	vi := val.Interface()
	switch val.Type().Kind() {
	case reflect.Slice:
		if val.Len() == 0 {
			return "[]"
		}
		if data, err := json.Marshal(vi); err == nil {
			return string(data)
		}
		return ""
	default:
		return fmt.Sprint(vi)
	}
}

func setStringValue(val reflect.Value, strVal string, converted bool) {
	if field, ok := castField(val); ok {
		field.SetString(strVal, converted)
		return
	}
	bVal := []byte(strVal)
	switch val.Interface().(type) {
	case string:
		val.SetString(strVal)
	case bool:
		strVal = strings.ToLower(strVal)
		ok := strVal == "true" || strVal == "1"
		val.SetBool(ok)
	case int, int16, int32, int64:
		if v, err := strconv.ParseInt(strVal, 10, 64); err == nil {
			val.SetInt(v)
		}
	case uint, uint16, uint32, uint64:
		if v, err := strconv.ParseUint(strVal, 10, 64); err == nil {
			val.SetUint(v)
		}
	case float32, float64:
		strVal = strings.ReplaceAll(strVal, ",", ".")
		if v, err := strconv.ParseFloat(strVal, 64); err == nil {
			val.SetFloat(v)
		}
	case []string:
		if strs := []string{}; json.Unmarshal(bVal, &strs) == nil {
			val.Set(reflect.ValueOf(strs))
		}
	case []int:
		if ints := []int{}; json.Unmarshal(bVal, &ints) == nil {
			val.Set(reflect.ValueOf(ints))
		}
	case []int16:
		if ints := []int16{}; json.Unmarshal(bVal, &ints) == nil {
			val.Set(reflect.ValueOf(ints))
		}
	case []int32:
		if ints := []int32{}; json.Unmarshal(bVal, &ints) == nil {
			val.Set(reflect.ValueOf(ints))
		}
	case []int64:
		if ints := []int64{}; json.Unmarshal(bVal, &ints) == nil {
			val.Set(reflect.ValueOf(ints))
		}
	case []uint:
		if ints := []uint{}; json.Unmarshal(bVal, &ints) == nil {
			val.Set(reflect.ValueOf(ints))
		}
	case []uint16:
		if ints := []uint16{}; json.Unmarshal(bVal, &ints) == nil {
			val.Set(reflect.ValueOf(ints))
		}
	case []uint32:
		if ints := []uint32{}; json.Unmarshal(bVal, &ints) == nil {
			val.Set(reflect.ValueOf(ints))
		}
	case []uint64:
		if ints := []uint64{}; json.Unmarshal(bVal, &ints) == nil {
			val.Set(reflect.ValueOf(ints))
		}
	case []float32:
		if ints := []float32{}; json.Unmarshal(bVal, &ints) == nil {
			val.Set(reflect.ValueOf(ints))
		}
	case []float64:
		if ints := []float64{}; json.Unmarshal(bVal, &ints) == nil {
			val.Set(reflect.ValueOf(ints))
		}
	case time.Duration:
		if d, err := time.ParseDuration(strVal); err == nil {
			val.Set(reflect.ValueOf(d))
		}
	}
}

type structField func(path string, isStruct bool, tag reflect.StructTag, value reflect.Value) error

var errScanStop = errors.New("scan stop")

func scanStruct(val interface{}, iter structField) error {
	return scanStructRecursive("", reflect.ValueOf(val).Elem(), iter)
}

func scanStructRecursive(prefix string, structValue reflect.Value, iter structField) error {
	t := structValue.Type()
	for i := 0; i < t.NumField(); i++ {
		fi := t.Field(i)
		ft := fi.Type
		vi := structValue.Field(i)

		if c := fi.Name[0]; c < 'A' || 'Z' < c /* is not exported */ {
			continue
		}

		if fi.Tag.Get("config") == "-" {
			continue
		}
		fname := utils.ToSnakeCase(fi.Name)

		switch ft.Kind() {
		case reflect.Func, reflect.Chan, reflect.UnsafePointer, reflect.Interface, reflect.Invalid:
		case reflect.Ptr:
			if ft.Elem().Kind() != reflect.Struct {
				break
			}
			if vi.IsNil() {
				vi.Set(reflect.New(ft.Elem()))
			}
			if vi.CanInterface() {
				if err := iter(prefix+fname, true, fi.Tag, vi); err != nil {
					return err
				}
			}
			vi = vi.Elem()
			fallthrough
		default:
			_, isCustomField := castField(vi)
			if isCustomField || vi.Kind() != reflect.Struct {
				if err := iter(prefix+fname, false, fi.Tag, vi); err != nil {
					return err
				}
			} else {
				if fi.Anonymous {
					fname = ""
				} else {
					fname += "."
				}
				if err := scanStructRecursive(prefix+fname, vi, iter); err != nil {
					return err
				}
			}
		}
	}
	return nil
}
